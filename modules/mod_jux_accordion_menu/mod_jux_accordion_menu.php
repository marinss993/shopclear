<?php
/**
 * @version		$Id: $
 * @author		JoomlaUX
 * @package		Joomla!
 * $subpackage	mod_jux_accordion_menu
 * @copyright	Copyright (C) 2013 JoomlaUX. All rights reserved.
 * @license		GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html, see LICENSE.txt
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once (dirname(__FILE__).'/helper.php');

$copy = array('a','b','c','d','e','f','g','h','i','j','k','l');
$suffix	= 'firstpane'.$copy[rand(0, count($copy)-1)];
$suffix .= $copy[rand(0, count($copy)-1)];
$suffix .= $copy[rand(0, count($copy)-1)];
$suffix .= $copy[rand(0, count($copy)-1)];
$suffix .= $copy[rand(0, count($copy)-1)];

$params->def('menusuffix', $suffix);

//Ensure for overflow memory
if (!$params->get('endLevel',	10)) {
	$params->set('endLevel',	10);
}

$starlevel		= $params->get('startLevel', 1);
$endlevel		= ($params->get('endLevel', 10)) ? $params->get('endLevel', 10) : $params->def('endLevel', 10);

$jux_accmenu = new JUXAccordionMenu ($params);

$document = JFactory::getDocument();
$document->addStyleSheet('modules/mod_jux_accordion_menu/assets/css/style.css');
require(JModuleHelper::getLayoutPath('mod_jux_accordion_menu'));