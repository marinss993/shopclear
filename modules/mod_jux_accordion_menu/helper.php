<?php
/**
 * @version		$Id: $
 * @author		JoomlaUX
 * @package		Joomla!
 * $subpackage	mod_jux_accordion_menu
 * @copyright	Copyright (C) 2013 JoomlaUX. All rights reserved.
 * @license		GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html, see LICENSE.txt
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.html.parameter' );

class JUXAccordionMenu extends JObject{
	var $_params			= null;
	var $children			= null;
	var $open				= null;
	var $items				= null;
	var $_start				= 1;

	function __construct( &$params ){
		$this->_params = $params;
		$this->loadMenu();
	}

	function createParameterObject($param) {
		// upgrade to joomla 3
		$params		= new JRegistry();
		$params->loadString($param);
		return $params;
	}

	function getPageTitle ($params) {
		return $params->get ('page_title');
		
	}
	
	function  loadMenu(){
		$list		= array();
		$db			= JFactory::getDbo();
		$user		= JFactory::getUser();
		$app		= JFactory::getApplication();
		$menu		= $app->getMenu();
		$children	= array ();

		//$aid 		= 1;
		$groups		= $user->getAuthorisedViewLevels();

		//start - end for fetching menu items
		$start 		= (int) $this->getParam('startlevel');
		if ($start < 1) $start = 1; //first level
		$this->_start = $start;

		// If no active menu, use default
		$active = ($menu->getActive()) ? $menu->getActive() : $menu->getDefault();

		$this->open		= isset($active) ? array_reverse($active->tree) : array();

		$rows 		= $menu->getItems('menutype',$this->getParam('menutype'));
		if(!count($rows)) return;
		// first pass - collect children
		$cacheIndex = array();
		$this->items = array();
		foreach ($rows as $index => $v) {
			$v->title = str_replace ('&', '&amp;', str_replace ('&amp;', '&', $v->title));
			if (in_array($v->access, $groups)) {
				$pt = $v->parent_id;
				$list = @ $children[$pt] ? $children[$pt] : array ();

				$v->flink = $v->link;
				switch ($v->type) {
					case 'separator':
						// No further action needed.
						break;

					case 'url':
						if ((strpos($v->link, 'index.php?') === 0) && (strpos($v->link, 'Itemid=') === false)) {
							// If this is an internal Joomla link, ensure the Itemid is set.
							$v->flink = $v->link.'&Itemid='.$v->id;
						}
						break;

					case 'alias':
						// If this is an alias use the item id stored in the parameters to make the link.
						$v->flink = 'index.php?Itemid='.$v->params->get('aliasoptions');
						//echo $v->params->get('aliasoptions'); exit();
						break;

					default:
						$router = JSite::getRouter();
						if ($router->getMode() == JROUTER_MODE_SEF) {
							$v->flink = 'index.php?Itemid='.$v->id;
						} else {
							$v->flink .= '&Itemid='.$v->id;
						}
						break;
				}
				$v->url = $v->flink = JRoute::_($v->flink);
				// Handle SSL links
				$iParams = $this->createParameterObject($v->params);
				$iSecure = $iParams->def('secure', 0);
				if ($v->home == 1) {
					$v->url = JURI::base();
				} elseif (strcasecmp(substr($v->url, 0, 4), 'http') && (strpos($v->link, 'index.php?') !== false)) {
					$v->url = JRoute::_($v->url, true, $iSecure);
				} else {
					$v->url = str_replace('&', '&amp;', $v->url);
				}

				$v->_idx = count($list);									
				array_push($list, $v);
				$children[$pt] = $list;
			}
			$cacheIndex[$v->id] = $index;
			$this->items[$v->id] = $v;
		}

		$this->children = $children;
	}

	function genMenuItem($item, $level = 1, $pos = '', $ret = 0) {
		$data = null;
		$tmp = $item;
		
		//if parent items link
		$parentlink	= (int) $this->_params->get('parentlink', 		1);
		$acclevels	= (int) $this->_params->get('accordionlevel', 	0);
		$endlevel	= (int) $this->_params->get('endLevel',			10);
		$notlink	= 0;
		if (!$parentlink) {
			if ( (($acclevels && $level <= $acclevels) || (!$acclevels)) && $level < $endlevel && @$this->children[$item->id]) {
				$tmp->url	= null;
			}
		}
		
		// Print a link if it exists
		$active = $this->genClass ($tmp, $level, $pos);

		$id='id="menu' . $tmp->id . '"';
		$title	= htmlspecialchars($tmp->params->get('menu-anchor_title', ''), ENT_COMPAT, 'UTF-8', false);
		$item->menu_image   = $item->params->get('menu_image', '') ? htmlspecialchars($item->params->get('menu_image', ''), ENT_COMPAT, 'UTF-8', false) : '';
		
		//echo $item->menu_image;
		// integrate
		if ($item->menu_image) {
				$item->params->get('menu_text', 1 ) ?
				$txt = '<span class="menu-image" style="background-image:url('.JUri::base().'/'.$item->menu_image.')"><span class="menu-title">'.$tmp->title.'</span></span>' :
				$txt = '<span class="menu-image no-title"><img src="'.$item->menu_image.'" alt="'.$tmp->title.'" /></span>';
		} else {
			$txt = '<span class="menu-title">' . $tmp->title . '</span>';
		}

		$title = "title=\"$title\"";
		if ($tmp->url != null)
		{
			switch ($tmp->browserNav)
			{
				default:
				case 0:
					// _top
					$data = '<a href="'.$tmp->url.'" '.$active.' '.$id.' '.$title.'>'.$txt.'</a>';
					break;
				case 1:
					// _blank
					$data = '<a href="'.$tmp->url.'" target="_blank" '.$active.' '.$id.' '.$title.'>'.$txt.'</a>';
					break;
				case 2:
					// window.open
					$attribs = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,'.$this->getParam('window_open');
					// hrm...this is a bit dickey
					$link = str_replace('index.php', 'index2.php', $tmp->url);
					$data = '<a href="'.$link.'" onclick="window.open(this.href,\'targetWindow\',\''.$attribs.'\');return false;" '.$active.' '.$id.' '.$title.'>'.$txt.'</a>';
					break;
			}
		} else {
			$data = '<a '.$active.' '.$id.' '.$title.'>'.$txt.'</a>';
		}
		if ($ret) return $data; else echo $data;
	}

	function getParam($paramName){
		return $this->_params->get($paramName);
	}

	function setParam($paramName, $paramValue){
		return $this->_params->set($paramName, $paramValue);
	}

	function beginMenu($startlevel=1, $endlevel = 10){
		echo "<div>";
	}
	function endMenu($startlevel=1, $endlevel = 10){
		echo "</div>";
	}

	function beginMenuItems($pid=1, $level=1){
		echo "<ul>";
	}
	function endMenuItems($pid=1, $level=1){
		echo "</ul>";
	}


	function genClass ($mitem, $level, $pos) {
		$active = in_array($mitem->id, $this->open);
		$cls = ($level?"":"menu-item{$mitem->_idx}"). ($active?" active":"").($pos?" $pos-item":"");
		return $cls?"class=\"$cls\"":"";
	}

	function hasSubMenu($level) {
		$pid = $this->getParentId ($level);
		if (!$pid) return false;
		return $this->hasSubItems ($pid);
	}
	function hasSubItems($id){
		if (@$this->children[$id]) return true;
		return false;
	}
	function genMenu($startlevel=1, $endlevel = 10){
		$this->setParam('startlevel', $startlevel);
		$this->setParam('endlevel', $endlevel);
		$this->beginMenu($startlevel, $endlevel);

		if ($this->getParam('startlevel') == 1) {
			//First level
			$this->genMenuItems (1, 1);
		}else{
			//Sub level
			$pid = $this->getParentId($this->getParam('startlevel'));
			//if ($pid)
			$this->genMenuItems ($pid, $this->getParam('startlevel'));
		}
		$this->endMenu($startlevel, $endlevel);
	}

	/*
	 $pid: parent id
	 $level: menu level
	 $pos: position of parent
	 */

	function genMenuItems($pid, $level) {
		if (@$this->children[$pid]) {
			$this->beginMenuItems($pid, $level);
			$i = 0;
			foreach ($this->children[$pid] as $row) {
				$pos = ($i == 0 ) ? 'first' : (($i == count($this->children[$pid])-1) ? 'last' :'');

				$this->beginMenuItem($row, $level, $pos);
				//$this->genMenuItem( $row, $level, $pos);
				// show menu with menu expanded - submenus visible
				if ($level < $this->getParam('endlevel')) $this->genMenuItems( $row->id, $level+1 );
				$i++;
				
				$this->endMenuItem($row, $level, $pos);
			}
			$this->endMenuItems($pid, $level);
		}
	}

	function indentText($level, $text) {
		echo "\n";
		for ($i=1;$i<$level;++$i) echo "   ";
		echo $text;
	}

	function getParentId ($level) {
		$level = $level - 1;
		if (!$level || (count($this->open) < $level)) return 0;
		return $this->open[count($this->open)-$level];
	}

	function getParentText ($level) {
		$pid = $this->getParentId ($level);
		if ($pid) {
			return $this->items[$pid]->name;
		}else return "";
	}
	
	function beginMenuItem($row=null, $level = 1, $pos = '') {
		$active = in_array($row->id, $this->open);
		$active = ($active) ? " active" : "";
		$acclevels	= (int) $this->_params->get('accordionlevel', 0);
		$endlevel	= (int) $this->_params->get('endLevel',	10);
		if ( $acclevels && $level <= $acclevels ) {
			$class	= "menu_head ";
		} elseif (!$acclevels) {
			$class	= "menu_head ";
		} else $class = "";
		if ($level == 1 && @$this->children[$row->id]) echo "<p class=\"{$class}level$level havechild{$active}\">";
		else if ($level > 1 && @$this->children[$row->id] && $level < $endlevel) echo "<p class=\"{$class}level$level havesubchild{$active}\">";
		else if ($level > 1 && @$this->children[$row->id]) echo "<p class=\"{$class}level$level\">";
		else echo "<p ".(($active) ? "class=\"{$class}level$level active\"" : "class=\"{$class}level$level\"").">";

		$this->genMenuItem( $row, $level, $pos);

		$liactive = in_array($row->id, $this->open);
		$style	= ($liactive && $this->_params->get('remind', 1)) ? 'style="display:block;"' : '';
		$acclevels	= (int) $this->_params->get('accordionlevel', 0);
		if ( (($acclevels && $level <= $acclevels) || !$acclevels) && @$this->children[$row->id] && $level < $endlevel) {
			echo "</p><div class=\"menu_body level$level \" $style>";
		}  else echo '';
	}

	function endMenuItem($mitem=null, $level = 1, $pos = ''){
		$endlevel	= (int) $this->_params->get('endLevel',	10);
		$acclevels	= (int) $this->_params->get('accordionlevel', 0);
		if ( (($acclevels && $level <= $acclevels) || !$acclevels) && @$this->children[$mitem->id] && $level < $endlevel) {
			echo "</div> \n";
		}  else echo "</p> \n";
	}

	function genInitMenu() {
		$suffix	= $this->_params->get('menusuffix');

		$suffix	= '"#'. $suffix.' p.menu_head"';
		$type	= $this->_params->get('type', 'hover');

		$multiopen 	= $this->_params->get('multiopen', 1) ? '' : '.siblings("div.menu_body").slideUp("slow")';
		$clickspeed	= $this->_params->get('clickspeed', 300);
		$hoverspeed	= $this->_params->get('hoverspeed', 500);
		// var_dump($multiopen); die;
		$toggle	= ($multiopen) ? ".slideDown($hoverspeed)" : ".slideToggle($clickspeed)";
		?>
		<script type="text/javascript">
		jQuery(document).ready(function($)
		{
			<?php if ( $type	== 'click' ) : ?>
			$(<?php echo $suffix; ?>).click(function()
			{
				$(this).next("div.menu_body").slideToggle(<?php echo $clickspeed; ?>)<?php echo $multiopen; ?>;
				<?php if (!$multiopen) { ?>

				if ($(this).hasClass('active') && ($(this).hasClass('havesubchild') || $(this).hasClass('havechild'))) {

					$(this).removeClass('active');
					$(this).next('div.menu_body').removeClass('active');

				} else if ($(this).hasClass('havesubchild') || $(this).hasClass('havechild')) {

					$(this).addClass('active');
					$(this).next('div.menu_body').addClass('active');

				}
				<?php } else { ?>
				if ($(this).hasClass('havesubchild') || $(this).hasClass('havechild')) {
					$(<?php echo $suffix;?>).removeClass('active');
					$(this).addClass('active');
					$(this).next('div.menu_body').addClass('active');
				}
				<?php } ?>
				// if ($(this).hasClass('havesubchild') || $(this).hasClass('havechild')) {
				// 	$(<?php echo $suffix;?>).removeClass('active');
				// 	$(this).addClass('active');
				// 	$(this).next('div.menu_body').addClass('active');
				// }
			});
			<?php else : ?>
			$(<?php echo $suffix; ?>).mouseover(function()
			{
				$(this).next("div.menu_body")<?php echo $toggle; ?><?php echo $multiopen; ?>;
				<?php if (!$multiopen) { ?>
				if ($(this).hasClass('active') && ($(this).hasClass('havesubchild') || $(this).hasClass('havechild'))) {
					$(this).removeClass('active');
				} else if ($(this).hasClass('havesubchild') || $(this).hasClass('havechild')) {
					$(this).addClass('active');
				}
				<?php } else {?>
				if ($(this).hasClass('havesubchild') || $(this).hasClass('havechild')) {
					$(<?php echo $suffix;?>).removeClass('active');
					$(this).addClass('active');
				}
				<?php } ?>
			});
			<?php endif; ?>
		});
		</script>
		<?php
	}
}