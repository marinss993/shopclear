<?php
/**
 * @version		$Id: $
 * @author		JoomlaUX
 * @package		Joomla!
 * $subpackage	mod_jux_accordion_menu
 * @copyright	Copyright (C) 2013 JoomlaUX. All rights reserved.
 * @license		GNU/GPL http://www.gnu.org/licenses/gpl-3.0.html, see LICENSE.txt
 * This file may not be redistributed in whole or significant part.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

global $module_baseurl;
	
	//make sure when copy module	
	$jux_accmenu->genInitMenu();
?>
<!--  BEGIN JUX ACCORDION MENU BLOCK -->
<div id="jux_accord-menu"><div id="<?php echo $suffix; ?>" class="menu_list">
	<?php $jux_accmenu->genMenu ($starlevel, $endlevel); ?>
</div></div>
<!--  END JUX ACCORDION MENU BLOCK -->
