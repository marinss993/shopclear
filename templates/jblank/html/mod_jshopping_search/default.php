<script type = "text/javascript">
function isEmptyValue(value){
    var pattern = /\S/;
    return ret = (pattern.test(value)) ? (true) : (false);
}
</script>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSearch" aria-controls="navbarSearch" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
 <div class="collapse navbar-collapse" id="navbarSearch">  

<form class="row mod-shop-search w-100" name = "searchForm" method = "post" action="<?php print SEFLink("index.php?option=com_jshopping&controller=search&task=result", 1);?>" onsubmit = "return isEmptyValue(jQuery('#jshop_search').val())">
<input type="hidden" name="setsearchdata" value="1">
<input type = "hidden" name = "category_id" value = "<?php print $category_id?>" />
<input type = "hidden" name = "search_type" value = "<?php print $search_type;?>" />
<input type = "text" class = "col-10 form-control" placeholder="<?php print _JSHOP_SEARCH?>" name = "search" id = "jshop_search" value = "<?php print htmlspecialchars($search)?>" />
<input class = "col-2 btn btn-outline-secondary" type = "submit" value = "<?php print _JSHOP_GO?>" />
<?php if ($adv_search) {?>
<div class="col-2 adv_search_link">
    <a class="btn btn-outline-info" href = "<?php print $adv_search_link?>"><?php print _JSHOP_ADVANCED_SEARCH?></a>
</div>
<?php } ?>
</form>


 
  </div>
</nav>