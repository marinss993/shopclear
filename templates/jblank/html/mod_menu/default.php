<?php
/**
package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later;
see LICENSE.txt
 */

defined('_JEXEC') or die;

$id = '';

if ($tagId = $params->get('tag_id', ''))
{
	$id = ' id="' . $tagId . '"';
}

// The menu class is deprecated. Use nav instead
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarMenu"> 
  <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
  <?php foreach ($list as $i => &$item){
    	$class = 'nav-item ' . $item->id;
    	
    	// if ($item->id == $default_id)
    	// 			{
    	// 	$class .= ' default';
    	// }
    	
    	if ($item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id))
            				{
    		$class .= ' current';
    	}
    	$item->anchor_css = 'nav-link ';
    	if (in_array($item->id, $path))
    				{
    		$class .= ' active';
    	}
    	elseif ($item->type === 'alias')
    				{
    		$aliasToId = $item->params->get('aliasoptions');
    		
    		if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
    								{
    			$class .= ' active';
    		}
    		elseif (in_array($aliasToId, $path))
    								{
    			$class .= ' alias-parent-active';
    		}
    	}
    	
    	if ($item->type === 'separator')
    				{
    		$class .= ' divider';
    	}
    	
    	if ($item->deeper)
    				{
    		$class .= ' deeper';
    	}
    	
    	if ($item->parent)
    				{
    		$class .= ' parent';
    	}
    	
    	echo '<li class="' . $class . '">';
    	
    	switch ($item->type) :
    					case 'separator':
    					case 'component':
    					case 'heading':
    					case 'url':
    						require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
    	break;
    	
    	default:
    						require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
    	break;
    	endswitch;
    	
    	// 	The next item is deeper.
    				if ($item->deeper)
    				{
    		echo '<ul class="nav-child unstyled small">';
    	}
    	// 	The next item is shallower.
    				elseif ($item->shallower)
    				{
    		echo '</li>';
    		echo str_repeat('</ul></li>', $item->level_diff);
    	}
    	// 	The next item is on the same level.
    				else
    				{
    		echo '</li>';
    	}
    }
    ?>
  </div>
</nav>