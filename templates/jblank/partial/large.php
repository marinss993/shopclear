<article class="container-fluid d-none d-lg-block d-xl-none">
    <!-- Шапка Сайта НАЧАЛО   -->
    <header class="row">
        <div class="col-3">

            <a title="<?php echo $sitename; ?>"
               href="<?php echo $_this->baseurl ?>">
                <img class="img w-100"
                     src="<?php echo JUri::root() . $_this->params->get('logoFile'); ?>"
                     alt="<?php echo $sitename; ?>"/>
                <?php if ($_this->countModules('main_logo')) { ?>
                    <jdoc:include type="modules" name="main_logo"/>
                <?php } ?>
            </a>
        </div>
        <div class="col-9">
            <div class="row">
                <div class="col-4">
                    <?php if ($_this->countModules('tel1')) { ?>
                        <jdoc:include type="modules" name="tel1"/>
                    <?php } else { ?>
                        <span><a href="tel: 0668595044">(xxx) xxx-xx-xx</a></span>
                    <?php } ?>
                </div>
                <div class="col-4">
                    <?php if ($_this->countModules('tel2')) { ?>
                        <jdoc:include type="modules" name="tel2"/>
                    <?php } else { ?>
                        <span><a href="tel: 0668595044">(xxx) xxx-xx-xx</a></span>
                    <?php } ?>
                </div>
                <div class="col-4">
                    <?php if ($_this->countModules('tel3')) { ?>
                        <jdoc:include type="modules" name="tel3"/>
                    <?php } else { ?>
                        <span><a href="tel: 0668595044">(xxx) xxx-xx-xx</a></span>
                    <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <?php if ($_this->countModules('menu')) { ?>
                        <jdoc:include type="modules" name="menu"/>
                    <?php } ?>
                </div>
            </div>
        </div>
    </header>
    <!-- Шапка Сайта КОНЕЦ   -->

    <!-- Поиск Начало   -->
    <section class="row justify-content-center">
        <div class="col-12">
            <?php if ($_this->countModules('search')) { ?>
                <jdoc:include type="modules" name="search"/>
            <?php } ?>
        </div>

    </section>
    <!-- Поиск КОНЕЦ   -->


    <!-- Главный Блок НАЧАЛО -->
    <main class="row overflow-auto">
        <div class="col-4"> 
            <?php if ($_this->countModules('categoriesMenu')) { ?>
                <jdoc:include type="modules" name="categoriesMenu"/>
            <?php } ?>
        </div>
        <div class="col-8">
            <jdoc:include type="component"/>
        </div>
      
    </main>
    <!-- Главный Блок КОНЕЦ -->

    <!--  Подвал НАЧАЛО  -->
   <!--  Подвал НАЧАЛО  -->
   <!-- <?php if ($_this->countModules('footer') || $_this->countModules('cart_ext')) { ?> -->
    <footer class="row ">
        <div class="col-12">
            <jdoc:include type="modules" name="cart_ext"/>
        </div>    
        <div class="col-12">
            <jdoc:include type="modules" name="footer"/>
        </div>
    </footer>
    <!-- <?php } ?> -->
    <!--  Подвал КОНЕЦ  -->
</article>


