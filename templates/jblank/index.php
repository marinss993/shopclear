<?php

/**
 * J!Blank Template for Joomla by JBlank.pro (JBZoo.com)
 *
 * @package    JBlank
 * @author     SmetDenis <admin@jbzoo.com>
 * @copyright  Copyright (c) JBlank.pro
 * @license    http://www.gnu.org/licenses/gpl.html GNU/GPL
 * @link       http://jblank.pro/ JBlank project page
 */

defined( '_JEXEC' ) or die;


// init $tpl helper
require dirname( __FILE__ ) . '/php/init.php';

?>
<!DOCTYPE html>
<?php echo $tpl->renderHTML(); ?>

<head>
    <script src="/templates/jblank/js/libs/jquery-3.4.0.min.js"></script>
    <script src="/templates/jblank/js/libs/bootstrap.bundle.min.js"></script>
    <jdoc:include type="head"/>
</head>

<body class="<?php echo $tpl->getBodyClasses(); ?> <?php echo $pageClassSuffix ?>">

<?php if ( $tpl->isMobile() ) { ?>

    <article class="container-fluid ">
        <header class="row">
            <div class="col-12">

				<?php if ( $this->countModules( 'tel1' ) ) { ?>
                    <jdoc:include type="modules" name="tel1"/>
				<?php } else { ?>
                    <span><a href="tel: 0668595044">(xxx) xxx-xx-xx</a></span>
				<?php } ?>
            </div>
            <div class="col-12">
				<?php if ( $this->countModules( 'tel2' ) ) { ?>
                    <jdoc:include type="modules" name="tel2"/>
				<?php } else { ?>
                    <span><a href="tel: 0668595044">(xxx) xxx-xx-xx</a></span>
				<?php } ?>
            </div>
            <div class="col-12">
				<?php if ( $this->countModules( 'tel3' ) ) { ?>
                    <jdoc:include type="modules" name="tel3"/>
				<?php } else { ?>
                    <span><a href="tel: 0668595044">(xxx) xxx-xx-xx</a></span>
				<?php } ?>
            </div>

            <div class="col-12">
				<?php if ( $this->countModules( 'menu' ) ) { ?>
                    <jdoc:include type="modules" name="menu"/>
				<?php } ?>
            </div>
        </header>
        <section class="col-7">
			<?php if ( $this->countModules( 'search' ) ) { ?>
                <jdoc:include type="modules" name="search"/>
			<?php } ?>
        </section>
        <main class="col-8">
            <jdoc:include type="component"/>
        </main>

    </article>

<?php } else { ?>
    <article class="container-fluid d-none d-lg-block">
        <!-- Шапка Сайта НАЧАЛО   -->
        <header class="row mt-lg-3 mt-xl-3">
            <div class="col-2">
                <a title="<?php echo $sitename; ?>" href="<?php echo $this->baseurl ?>">
                    <img class="img w-100" src="<?php echo JUri:: root() . $this->params->get( 'logoFile' ); ?>"
                         alt="<?php echo $sitename; ?>"/>
					<?php if ( $this->countModules( 'main_logo' ) ) { ?>
                        <jdoc:include type="modules" name="main_logo"/>
					<?php } ?>
                </a>
            </div>
            <div class="col-10">
                <div class="row ">
                    <div class="col-4">
						<?php if ( $this->countModules( 'tel1' ) ) { ?>
                            <jdoc:include type="modules" name="tel1"/>
						<?php } else { ?>
                            <span><a href="tel: 0668595044">(xxx) xxx-xx-xx</a></span>
						<?php } ?>
                    </div>
                    <div class="col-4">
						<?php if ( $this->countModules( 'tel2' ) ) { ?>
                            <jdoc:include type="modules" name="tel2"/>
						<?php } else { ?>
                            <span><a href="tel: 0668595044">(xxx) xxx-xx-xx</a></span>
						<?php } ?>
                    </div>
                    <div class="col-4">
						<?php if ( $this->countModules( 'tel3' ) ) { ?>
                            <jdoc:include type="modules" name="tel3"/>
						<?php } else { ?>
                            <span><a href="tel: 0668595044">(xxx) xxx-xx-xx</a></span>
						<?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
						<?php if ( $this->countModules( 'menu' ) ) { ?>
                            <jdoc:include type="modules" name="menu"/>
						<?php } ?>
                    </div>
                </div>
            </div>
        </header>
        <!-- Шапка Сайта КОНЕЦ   -->

        <!-- Поиск Начало   -->
        <section class="row  mt-lg-2 mt-xl-2">
            <div class="offset-2 col-7">
				<?php if ( $this->countModules( 'search' ) ) { ?>
                    <jdoc:include type="modules" name="search"/>
				<?php } ?>
            </div>
        </section>
        <!-- Поиск КОНЕЦ   -->


        <!-- Главный Блок НАЧАЛО -->
        <main class="row mt-lg-2 mt-xl-2">
            <div class="col-2">
				<?php if ( $this->countModules( 'categoriesMenu' ) ) { ?>
                    <jdoc:include type="modules" name="categoriesMenu"/>
				<?php } ?>
            </div>
            <div class="col-7">
                <jdoc:include type="component"/>
            </div>
            <div class="col-3">
				<?php if ( $this->countModules( 'cart_ext' ) ) { ?>
                    <jdoc:include type="modules" name="cart_ext"/>
				<?php } ?>
            </div>
        </main>
        <!-- Главный Блок КОНЕЦ -->

        <!--  Подвал НАЧАЛО  -->

		<?php if ( $this->countModules( 'footer' ) ) { ?>
            <footer class="row  mt-lg-2 mt-xl-2">
                <div class="col-12">

                    <jdoc:include type="modules" name="footer"/>

                </div>
            </footer>
		<?php } ?>
        <!--  Подвал КОНЕЦ  -->
    </article>
<?php } ?>


<?php if ( $tpl->isDebug() ) : ?>
    <jdoc:include type="modules" name="debug"/>
<?php endif; ?>

</body>

</html>