// check and define $ as jQuery
if (typeof jQuery != "undefined") jQuery(function ($) {
    
    // dump(myVar); is wrapper for console.log() with check existing console object and show 
    window.dump=function(vars,name,showTrace){if(typeof console=="undefined")return false;if(typeof vars=="string"||typeof vars=="array")var type=" ("+typeof vars+", "+vars.length+")";else var type=" ("+typeof vars+")";if(typeof vars=="string")vars='"'+vars+'"';if(typeof name=="undefined")name="..."+type+" = ";else name+=type+" = ";if(typeof showTrace=="undefined")showTrace=false;console.log(name,vars);if(showTrace)console.trace();return true};

    // remove no-js class if JavaScript enabled
    $('html.no-js').removeClass('no-js').addClass('js-ready');

    // close Joomla system messages (just example)
    $('#system-message .close').click(function () {
        $(this).closest('.alert').animate({height: 0, opacity: 0, MarginBottom: 0}, 'slow', function () {
            $(this).remove();
        });
        return false;
    });
   
    function num_click(e) {
        var el = e ? e.target : window.event.srcElement;
        if (el.tagName !== "SPAN") return;
        var inp = this.lastChild;
        var val = +inp.previousElementSibling.value;
        inp.previousElementSibling.value = val +(el.className == "plus" ? 1: val > 0 ? -1 : 0);

        var url_el = 'productlink' + inp.previousElementSibling.id;
        url_el.href=\''.$view->rows[$key]->buy_link.'&quantity=\'+qty_el.value;


        reloadPriceInList(inp.previousElementSibling.id, inp.previousElementSibling.value);
    };

    function num_input(e) {
        var el = e ? e.target : window.event.srcElement;
        if (el.tagName !== "INPUT") return;
        var val = el.value.replace(/\D/g, '');
        el.value = val ? val : 0;
    };
    document.querySelectorAll('div.choice').forEach(function (el) {
        el.onclick = num_click;
        el.oninput = num_input;
    });
    function reloadPriceInList(product_id, qty){
		var data = {};
		data["change_attr"] = 0;
		data["qty"] = qty;
		if (prevAjaxHandler){
			prevAjaxHandler.abort();
		}
		prevAjaxHandler = jQuery.getJSON(
			"index.php?option=com_jshopping&controller=product&task=ajax_attrib_select_and_price&product_id=" + product_id + "&ajax=1",
				data,
				function(json){
					jQuery(".product.productitem_"+product_id+" .jshop_price span").html(json.price);
				}
		);
	};
    // your JavaScript and jQuery code here
    // alert('JS Test!');

});
