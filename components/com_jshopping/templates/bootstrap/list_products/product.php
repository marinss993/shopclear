<?php
/**
 * @version      4.8.0 05.11.2013
 * @author       MAXXmarketing GmbH
 * @package      Jshopping
 * @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
 * @license      GNU/GPL
 */
defined( '_JEXEC' ) or die( 'Restricted access' );

?>
<?php print $product->_tmp_var_start ?>
<!-- components\com_jshopping\templates\bootstrap\list_products\product.php -->

<div class="row align-items-center">
    <div class="col-7 col-lg-7 col-xl-7">
        <a href="<?php print $product->product_link ?>">
			<?php print $product->name ?>
        </a>
    </div>
    <div class="col-1 col-lg-2 col-xl-2 justify-content-end">
		<?php if ( $product->product_price_default > 0 && $this->config->product_list_show_price_default ) { ?>
            <div class="default_price">
				<?php print _JSHOP_DEFAULT_PRICE . ": "; ?>
                <span><?php print formatprice( $product->product_price_default ) ?></span>
            </div>
		<?php } ?>

		<?php if ( $product->_display_price ) { ?>
            <div class="jshop_price" style="text-align: right">
				<?php if ( $this->config->product_list_show_price_description ) {
					print _JSHOP_PRICE . ": ";
				} ?>
				<?php if ( $product->show_price_from ) {
					print _JSHOP_FROM . " ";
				} ?>
                <span><?php print formatprice( $product->product_price ); ?><?php print $product->_tmp_var_price_ext; ?></span>
            </div>
		<?php } ?>

		<?php print $product->_tmp_var_bottom_price; ?>
    </div>
    <div class="col-2 col-lg-1 col-xl-1 ">

        <?php print $product->_tmp_var_buttons; ?>
        <?php print $product->_tmp_var_bottom_buttons; ?>

    </div>
    <div class="col-2 col-lg-2 col-xl-2">
		<?php print $product->_tmp_var_top_buttons; ?>
		<?php if ( $product->buy_link ) { ?>

            <a class="btn btn-outline-success button_buy w-100 " href="<?php print $product->buy_link ?>">
				<?php print _JSHOP_BUY ?>
            </a>
		<?php } ?>
    </div>

</div>

<?php print $product->_tmp_var_top_buttons; ?>



<?php print $product->_tmp_var_bottom_buttons; ?>


<?php print $product->_tmp_var_end ?>


</script>