<?php 
/**
* @version      4.8.0 13.08.2013
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');
?>
<!-- C:\server\www\shopclear\components\com_jshopping\templates\bootstrap\list_products\list_products.php -->
<div class="jshop list_product col-12" id="comjshop_list_product">
    <div class="row">
    <?php foreach ($this->rows as $k=>$product) : ?>
    <?php if ($k % $this->count_product_to_row == 0) : ?>
        <!-- <div class = "row-fluid"> -->
    <?php endif; ?>
    
    <div class = "col-12 block_product">
        <?php include(dirname(__FILE__)."/".$product->template_block_product);?>
    </div>
            
<?php endforeach; ?>
<?php if ($k % $this->count_product_to_row != $this->count_product_to_row - 1) : ?>
    
    </div>
<?php endif; ?>
    </div>

</div>