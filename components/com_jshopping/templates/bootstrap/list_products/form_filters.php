<?php 
/**
* @version      4.8.0 13.08.2013
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light w-100">
  <!-- <a class="navbar-brand" href="#">Фильтр</a> -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarFilter" aria-controls="navbarFilter" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarFilter">

<form class="my-2 my-lg-0 w-100" action="<?php print $this->action;?>" method="post" name="sort_count" id="sort_count" class="form-horizontal">
<?php if ($this->config->show_sort_product || $this->config->show_count_select_products) : ?>
<!-- <div class="block_sorting_count_to_page"> -->
    <?php if ($this->config->show_sort_product) : ?>
        <!-- <div class="control-group box_products_sorting"> -->
            <!-- <div class="control-label"> -->

<div class="form-row align-items-center">
    <div class="col-auto">  
      <div class="input-group mb-2">
        <label for="order" class="ml-2 mr-2" for="order"><?php print _JSHOP_ORDER_BY.": "; ?></label> 
        <div class="input-group-prepend">
          <div class="input-group-text"><img src="<?php print $this->path_image_sorting_dir?>" alt="orderby" onclick="submitListProductFilterSortDirection()" /></div>
        </div>
        <?php echo $this->sorting?>
      </div>
    </div>
   <?php endif; ?>
    <?php if ($this->config->show_count_select_products) : ?>
         <div class="col-auto"> 
             <div class="input-group mb-2">    
                 <label for="Limit" class="ml-2 mr-2"><?php print _JSHOP_DISPLAY_NUMBER.": "; ?></label>
                
                     <?php echo $this->product_count?>
                
             </div>
         </div>
    <?php endif; ?>
<?php endif; ?> 
    
    
   
        

</div>




    

<?php if ($this->config->show_product_list_filters && $this->filter_show) : ?>

    <?php if ($this->config->show_sort_product || $this->config->show_count_select_products) : ?>
        <div class="margin_filter"></div>
    <?php endif; ?>
    
    <div class="jshop filters">
        <div class="box_cat_man">
            <?php if ($this->filter_show_category) : ?>
                <div class = "control-group box_category">
                    <div class = "control-label">
                        <?php print _JSHOP_CATEGORY.": "; ?>
                    </div>
                    <div class = "controls"><?php echo $this->categorys_sel?></div>
                </div>
            <?php endif; ?>
            <?php if ($this->filter_show_manufacturer) : ?>
                <div class="control-group box_manufacrurer">
                    <div class="control-label">
                        <?php print _JSHOP_MANUFACTURER.": "; ?>
                    </div>
                    <div class="controls">
                        <?php echo $this->manufacuturers_sel; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php print $this->_tmp_ext_filter_box;?>
        </div>
        
        <?php if (getDisplayPriceShop()) : ?>
            <div class="filter_price">
                <label><?php print _JSHOP_PRICE?>: </label>
                <div class="control-group box_price_from">
                    <div class="control-label"><?php print _JSHOP_FROM?></div>
                    <div class="controls">
                        <span class="input-append">
                            <input type="text" class="input" name="fprice_from" id="price_from" size="7" value="<?php if ($this->filters['price_from']>0) print $this->filters['price_from']?>" />
                            <a class="btn"><?php print $this->config->currency_code?></a>
                        </span>
                    </div>

                </div>
                <div class="control-group box_price_to">
                    <div class="control-label"><?php print _JSHOP_TO?></div>
                    <div class="controls">
                        <span class="input-append">
                            <input type="text" class="input" name="fprice_to"  id="price_to" size="7" value="<?php if ($this->filters['price_to']>0) print $this->filters['price_to']?>" />
                            <a class="btn"><?php print $this->config->currency_code?></a>
                        </span>
                    </div>
                </div>
                <?php print $this->_tmp_ext_filter;?>
                <div class="control-group box_button">
                    <div class="controls">
                    <input type="button" class="btn button" value="<?php print _JSHOP_GO; ?>" onclick="submitListProductFilters();" />
                    <span class="clear_filter"><a href="#" onclick="clearProductListFilter();return false;"><?php print _JSHOP_CLEAR_FILTERS?></a></span>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>

<input type="hidden" name="orderby" id="orderby" value="<?php print $this->orderby?>" />
<input type="hidden" name="limitstart" value="0" />
</form>

  </div>
</nav>

