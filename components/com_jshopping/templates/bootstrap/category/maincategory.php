<?php 
/**
* @version      4.8.0 13.08.2013
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');
?>
<?php if ($this->params->get('show_page_heading') && $this->params->get('page_heading')){?>
<div class="col-12 shophead<?php print $this->params->get('pageclass_sfx');?>">
    <h1><?php print $this->params->get('page_heading')?></h1>
</div>
<?php }?>
<!-- C:\server\www\shopclear\components\com_jshopping\templates\bootstrap\category\maincategory.php -->
<div class="jshop row" id="comjshop">
    <div class="category_description col-12">
        <?php print $this->category->description?>
    </div>

    <div class="jshop_list_category col-12">
    <?php if (count($this->categories)) : ?>
        <?php foreach ($this->categories as $k => $category) : ?>
            <?php if ($k % $this->count_category_to_row == 0) : ?>
                <div class = "row">
            <?php endif; ?>
        
            <div class = "col-1 col-sm-2 col-md-3 col-lg-4 col-xl-4; ?> jshop_categ category">
                <div class = "span<?php echo 12 - 12 / $this->count_category_to_row; ?> image">
                    <a href = "<?php print $category->category_link;?>">
                        <img class = "jshop_img img-fluid" src = "<?php print $this->image_category_path;?>/<?php if ($category->category_image) print $category->category_image; else print $this->noimage;?>" alt="<?php print htmlspecialchars($category->name);?>" title="<?php print htmlspecialchars($category->name);?>" />
                    </a>
                </div>
                <div class = "span<?php echo 12 / $this->count_category_to_row; ?>">
                    <div class="category_name">
                        <a class = "product_link" href = "<?php print $category->category_link?>">
                            <?php print $category->name?>
                        </a>
                    </div>
                    <p class = "category_short_description">
                        <?php print $category->short_description?>
                    </p>
                </div>
            </div>
            
            <?php if ($k % $this->count_category_to_row == $this->count_category_to_row - 1) : ?>
                <div class = "clearfix"></div>
                </div>
            <?php endif; ?>
        <?php endforeach;?>
        <?php if ($k % $this->count_category_to_row != $this->count_category_to_row - 1) : ?>
            <div class = "clearfix"></div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    </div>
</div>