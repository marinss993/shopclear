<?php 
/**
* @version      4.8.0 13.08.2013
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');
?>
<!-- components\com_jshopping\templates\bootstrap\category\category_default.php -->
<div class="row 1 jshop" id="comjshop">
  <div class="col-12"><h1><?php print $this->category->name?></h1></div>
  <div class="col-12">
    <div class="category_description">
        <?php print $this->category->description?>
    </div>
  </div>
    <div class="col-12 jshop_list_category">
        <div class = "row jshop list_category">
        <?php if (count($this->categories)) : ?>
            <?php foreach($this->categories as $k=>$category) : ?>
                <div class = "col-12 col-sm-2 col-md-3 col-lg-3 col-xl-3">
                    <div class="card">
                      <a href = "<?php print $category->category_link;?>">
                          <img class="jshop_img card-img-top" src="<?php print $this->image_category_path;?>/<?php if ($category->category_image) print $category->category_image; else print $this->noimage;?>" alt="<?php print htmlspecialchars($category->name)?>" title="<?php print htmlspecialchars($category->name)?>" />
                      </a>
                      <div class="card-body">
                          <h5 class="card-title category_name">
                          <a class = "product_link" href = "<?php print $category->category_link?>">
                                <?php print $category->name?>
                          </a>
                          <p class = "category_short_description card-text"><?php print $category->short_description?></p>  
                      </div>
                    </div>

                </div>
                <?php if ($k % $this->count_category_to_row == $this->count_category_to_row - 1) : ?>
                    <div class = "clearfix"></div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
                <?php if ($k % $this->count_category_to_row != $this->count_category_to_row - 1) : ?>
                    <div class = "clearfix"></div>        
                <?php endif; ?>
        <?php endif; ?>
        </div>
    </div>
    <?php include(dirname(__FILE__)."/products.php");?>
</div>


