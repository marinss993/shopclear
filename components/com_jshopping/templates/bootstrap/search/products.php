<?php 
/**
* @version      4.3.1 13.08.2013
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');
?>
<!-- C:\server\www\shopclear\components\com_jshopping\templates\bootstrap\search\products.php -->
<div class="jshop row" id="comjshop">
    <div class="col-12">
        <h1><?php print _JSHOP_SEARCH_RESULT?> <?php if ($this->search) print '"'.$this->search.'"';?></h1>
    </div>
    <?php if (count($this->rows)){ ?> 
        <div class="col-12">
            <?php include(dirname(__FILE__)."/../".$this->template_block_form_filter);?>
        </div>
        <?php if (count($this->rows)){ ?>
            <div class="col-12">
                <?php include(dirname(__FILE__)."/../".$this->template_block_list_product);?>
            </div>
        <?php } ?>
        <?php if ($this->display_pagination){ ?>
            <div class="col-12">
                <?php include(dirname(__FILE__)."/../".$this->template_block_pagination); ?>
            </div>
        <?php } ?>
    <?php } ?>
</div>